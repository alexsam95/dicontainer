using SimpleDIContainer.UserLayer;

namespace SimpleDIContainer.CoreLogic.Strategy
{
    internal interface ILifecycleStrategyFactory
    {
        ILifecycleStrategy GetStrategy(LifecycleType lifecycleType);
    }
}