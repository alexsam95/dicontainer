using System;

namespace SimpleDIContainer.CoreLogic.Strategy
{
    public interface ILifecycleStrategy
    {
        object GetInstance(Type type, ObjectDataContainer objectDataContainer);
    }
}