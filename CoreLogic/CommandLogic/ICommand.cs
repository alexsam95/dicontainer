using System;
using System.Collections.Generic;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.CoreLogic.Strategy;

namespace SimpleDIContainer.CoreLogic.CommandLogic
{
    public interface ICommand
    {
        string PropertyBindName { set; }
        string BindMethodName { set; }
        PropertyResolveMode PropertyResolveMode { set; }
        ILifecycleStrategy LifecycleStrategy { set; }
        IList<Type> AdditionalTypes { get; set; }
        void AddObjectsForConstructor(params object[] parameters);
        void AddObjectsForMethod(string methodName, params object[] parameters);
        void AddObjectsForProperty(string propertyName, object parameter);
        void Invoke(bool withInstance);
    }
}