using System;
using System.Collections.Generic;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.CoreLogic.Strategy;

namespace SimpleDIContainer.CoreLogic.CommandLogic
{
    /// <summary>
    /// Null object 
    /// </summary>
    internal class EmptyCommand : ICommand
    {
        public string PropertyBindName { get; set; }
        public string BindMethodName { get; set; }
        public PropertyResolveMode PropertyResolveMode { get; set; }
        public ILifecycleStrategy LifecycleStrategy { get; set; }
        public IList<Type> AdditionalTypes { get; set; } = new List<Type>();

        public void AddObjectsForConstructor(params object[] parameters)
        {

        }

        public void AddObjectsForMethod(string methodName, params object[] parameters)
        {
        }

        public void AddObjectsForProperty(string propertyName, object parameter)
        {
        }

        public void Invoke(bool withInstance)
        {
        }
    }
}