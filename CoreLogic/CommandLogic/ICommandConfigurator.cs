using System;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.UserLayer;

namespace SimpleDIContainer.CoreLogic.CommandLogic
{
    public interface ICommandConfigurator : IDisposable
    {
        void AddCommandToChange(ICommand command);
        void AddAdditionalType<TAbstractType>();
        void AddPropertyTypeForBind(string propertyName);
        void AddMethodNameForBind(string methodName);
        void SetLifecycleType(LifecycleType type);
        void SetPropertyResolveMode(PropertyResolveMode mode);
        void AddObjectsForConstructor(params object[] parameters);
        void AddObjectsForMethod(string methodName, params object[] parameters);
        void AddObjectsForProperty(string propertyName, object parameters);
    }
}