using System;
using System.Collections.Generic;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.CoreLogic.Strategy;
using SimpleDIContainer.DataLayer;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.CoreLogic.CommandLogic
{
    internal class Command : ICommand
    {
        private readonly ObjectDataContainer _objectDataContainer;
        private readonly IObjectsStorage<Type, ObjectDataContainer> _objectsStorage;
        private readonly Type _registerType;

        internal Command(Type registerType, IObjectsStorage<Type, ObjectDataContainer> storage,
            ObjectDataContainer objectDataContainer, ILifecycleStrategy defaultLifecycleStrategy)
        {
            VerificationExtensions.CheckParametersForNull(() =>
                    throw new ContainerException("Command null arguments in constructor", false),
                registerType, storage, objectDataContainer);
            _registerType = registerType;
            _objectsStorage = storage;
            _objectDataContainer = objectDataContainer;
            if (_objectDataContainer.LifecycleStrategy == null)
            {
                _objectDataContainer.LifecycleStrategy = defaultLifecycleStrategy;
            }
        }

        public string PropertyBindName
        {
            set => _objectDataContainer.ResolveParameters?.PropertyTypeList.Add(value);
        }

        public string BindMethodName
        {
            set => _objectDataContainer.ResolveParameters?.MethodNames.Add(value);
        }

        public PropertyResolveMode PropertyResolveMode
        {
            set => _objectDataContainer.ResolveParameters.PropertyResolveMode = value;
        }

        public ILifecycleStrategy LifecycleStrategy
        {
            set => _objectDataContainer.LifecycleStrategy = value;
        }

        public IList<Type> AdditionalTypes { get; set; } = new List<Type>();

        public void AddObjectsForConstructor(params object[] parameters)
        {
            _objectDataContainer.ObjectsForConstructor.AddRange(parameters);
        }

        public void AddObjectsForMethod(string methodName, params object[] parameters)
        {
            var objectsList = new List<object>();
            objectsList.AddRange(parameters);
            _objectDataContainer.ObjectsForMethod.Add(methodName, objectsList);
            _objectDataContainer.ResolveParameters.MethodNames.Add(methodName);
        }

        public void AddObjectsForProperty(string propertyName, object parameter)
        {
            _objectDataContainer.ObjectsForProperty.Add(propertyName, parameter);
            _objectDataContainer.ResolveParameters.MethodNames.Add(propertyName);
        }

        public void Invoke(bool withInstance)
        {
            _objectDataContainer.AdditionalTypesForRegister.AddRange(AdditionalTypes);
            _objectsStorage.Add(_registerType, _objectDataContainer);
            if (withInstance)
            {
                var unused = _objectDataContainer.ObjectInstance;
            }
        }
    }
}