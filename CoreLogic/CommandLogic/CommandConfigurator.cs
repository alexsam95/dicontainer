using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.CoreLogic.Strategy;
using SimpleDIContainer.UserLayer;

namespace SimpleDIContainer.CoreLogic.CommandLogic
{
    internal class CommandConfigurator : ICommandConfigurator
    {
        private ICommand _command = new EmptyCommand();
        private readonly ILifecycleStrategyFactory _lifecycleStrategyFactory;

        public CommandConfigurator(ILifecycleStrategyFactory lifecycleStrategyFactory)
        {
            _lifecycleStrategyFactory = lifecycleStrategyFactory;
        }

        public void AddCommandToChange(ICommand command)
        {
            _command = command;
        }

        public void AddAdditionalType<TAbstractType>()
        {
            _command.AdditionalTypes.Add(typeof(TAbstractType));
        }

        public void SetLifecycleType(LifecycleType type)
        {
            _command.LifecycleStrategy = _lifecycleStrategyFactory.GetStrategy(type);
        }

        public void SetPropertyResolveMode(PropertyResolveMode mode)
        {
            _command.PropertyResolveMode = mode;
        }

        public void AddObjectsForConstructor(params object[] parameters)
        {
            _command.AddObjectsForConstructor(parameters);
        }

        public void AddObjectsForMethod(string methodName, params object[] parameters)
        {
            _command.AddObjectsForMethod(methodName, parameters);
        }

        public void AddPropertyTypeForBind(string propertyName)
        {
            _command.PropertyBindName = propertyName;
        }

        public void AddMethodNameForBind(string methodName)
        {
            _command.BindMethodName = methodName;
        }

        public void AddObjectsForProperty(string propertyName, object parameter)
        {
            _command.AddObjectsForProperty(propertyName, parameter);
        }

        public void Dispose()
        {
            _command = new EmptyCommand();
        }
    }
}