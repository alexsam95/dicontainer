using System;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.UserLayer;

namespace SimpleDIContainer.CoreLogic.CommandLogic
{
    public interface ICommandStorage : IDisposable
    {
        void AddCommand(Type abstractType, Type concreteType);
        void AddCommand(Type abstractType, object instance);
        void RunCommands();
    }
}