using System;
using System.Collections.Generic;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.CoreLogic.Strategy;
using SimpleDIContainer.DataLayer;
using SimpleDIContainer.SystemLayer.Extensions;
using SimpleDIContainer.UserLayer;

namespace SimpleDIContainer.CoreLogic.CommandLogic
{
    internal class CommandStorage : ICommandStorage
    {
        private readonly List<ICommand> _commands = new List<ICommand>();
        private readonly IObjectsStorage<Type, ObjectDataContainer> _storage;
        private readonly ILifecycleStrategyFactory _lifecycleStrategyFactory;
        private readonly ICommandConfigurator _commandConfigurator;
        private ICommand _inProgressCommand = new EmptyCommand();

        internal CommandStorage(IObjectsStorage<Type, ObjectDataContainer> storage,
            ILifecycleStrategyFactory lifecycleStrategyFactory, ICommandConfigurator commandConfigurator)
        {
            VerificationExtensions.CheckParametersForNull(() => throw new ContainerException
                    ("null arguments " + "in constructorCommandController", false),
                storage, lifecycleStrategyFactory);
            _commandConfigurator = commandConfigurator;
            _lifecycleStrategyFactory = lifecycleStrategyFactory;
            _storage = storage;
        }

        public void RunCommands()
        {
            _commands.ForEach(command => { command.Invoke(command == _inProgressCommand); });
            _inProgressCommand = new EmptyCommand();
            _commandConfigurator.Dispose();
            _commands.Clear();
        }

        public void AddCommand(Type abstractType, Type concreteType)
        {
            var command = new Command(abstractType, _storage,
                new ObjectDataContainer(concreteType,
                    new ResolveParameters()), _lifecycleStrategyFactory.GetStrategy(LifecycleType.Singleton));
            _commands.Add(command);
            _commandConfigurator.AddCommandToChange(command);
            _inProgressCommand = command;
        }

        public void AddCommand(Type abstractType, object instance)
        {
            var command = new Command(abstractType, _storage,
                new ObjectDataContainer(instance), _lifecycleStrategyFactory.GetStrategy(LifecycleType.Singleton));
            _commands.Add(command);
            _commandConfigurator.AddCommandToChange(command);
            _inProgressCommand = command;
        }
        public void Dispose()
        {
            _commandConfigurator.Dispose();
            _inProgressCommand = null;
            _commands.Clear();
        }
    }
}