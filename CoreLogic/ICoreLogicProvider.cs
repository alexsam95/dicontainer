using System;
using SimpleDIContainer.CoreLogic.CommandLogic;
using SimpleDIContainer.CoreLogic.Strategy;

namespace SimpleDIContainer.CoreLogic
{
    internal interface ICoreLogicProvider : IDisposable
    {
        IActivatorService ActivatorService { get; }
        ILifecycleStrategyFactory LifecycleStrategyFactory { get; }
        ICommandStorage CommandStorage { get; }
        ICommandConfigurator CommandConfigurator { get; }
    }
}
