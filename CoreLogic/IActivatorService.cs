using System;

namespace SimpleDIContainer.CoreLogic
{
    internal interface IActivatorService
    {
        object CreateObject(Type type, ObjectDataContainer objectDataContainer);
    }
}