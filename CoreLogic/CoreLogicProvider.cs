using System;
using System.Collections.Generic;
using SimpleDIContainer.CoreLogic.CommandLogic;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.CoreLogic.Strategy;
using SimpleDIContainer.DataLayer;
using SimpleDIContainer.SystemLayer.Extensions;
using SimpleDIContainer.Utils;

namespace SimpleDIContainer.CoreLogic
{
    internal class CoreLogicProvider : ICoreLogicProvider
    {
        private readonly Lazy<IActivatorService> _activatorService;
        private readonly IDataLayerProvider _dataLayerProvider;
        private readonly Lazy<ILifecycleStrategyFactory> _lifecycleStrategyFactory;
        private readonly Lazy<ICommandStorage> _commandStorage;
        private readonly Lazy<ICommandConfigurator> _commandConfigurator;
        private IEnumerable<IPreInstanceResolver> _preInstanceResolvers;
        public ICommandConfigurator CommandConfigurator => _commandConfigurator.Value;
        public ILifecycleStrategyFactory LifecycleStrategyFactory => _lifecycleStrategyFactory.Value;
        public ICommandStorage CommandStorage => _commandStorage.Value;

        public IActivatorService ActivatorService => _activatorService.Value;

        public CoreLogicProvider(IDataLayerProvider dataLayerProvider, ILoggerProvider loggerProvider)
        {
            VerificationExtensions.CheckParametersForNull(() =>
                    throw new ContainerException("CoreLogicProvider constructor argument is null", false)
                , dataLayerProvider);
            _dataLayerProvider = dataLayerProvider;
            _lifecycleStrategyFactory =
                new Lazy<ILifecycleStrategyFactory>(() => new LifeCycleStrategyFactory(_activatorService.Value));
            _commandConfigurator =
                new Lazy<ICommandConfigurator>(() => new CommandConfigurator(LifecycleStrategyFactory));
            _commandStorage = new Lazy<ICommandStorage>(() =>
                new CommandStorage(_dataLayerProvider.ObjectStorage, _lifecycleStrategyFactory.Value,
                    _commandConfigurator.Value));
            _activatorService = new Lazy<IActivatorService>(() =>
            {
                _preInstanceResolvers = new[]
                {
                    new PreInstanceResolverLogDecorator(new ConstructorPreInstanceResolver(
                        _dataLayerProvider.ObjectStorage),loggerProvider)
                    
                };
                var postInstanceResolvers = new IPostInstanceResolver[]
                {
                    new PostInstanceResolverLogDecorator(new PropertyPreInstanceResolver(_dataLayerProvider.ObjectStorage),loggerProvider),
                    new PostInstanceResolverLogDecorator(new MethodPreInstanceResolver(_dataLayerProvider.ObjectStorage),loggerProvider),
                    new PostInstanceResolverLogDecorator(new FactoryResolver(_dataLayerProvider.ObjectStorage), loggerProvider)
                };
                return new ActivatorErrorHandleDecorator(new ActivatorService(_preInstanceResolvers,
                    postInstanceResolvers));
            });
        }

        public void Dispose()
        {
            _commandStorage.Value?.Dispose();
            _preInstanceResolvers.ForEach(resolver => resolver.Dispose());
        }
    }
}