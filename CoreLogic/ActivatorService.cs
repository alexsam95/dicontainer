using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.CoreLogic
{
    internal class ActivatorService : IActivatorService
    {
        private readonly IEnumerable<IPreInstanceResolver> _preInstanceResolvers;
        private readonly IEnumerable<IPostInstanceResolver> _postInstanceResolvers;

        internal ActivatorService(IEnumerable<IPreInstanceResolver> preInstanceResolvers,
            IEnumerable<IPostInstanceResolver> postInstanceResolvers)
        {
            _postInstanceResolvers = postInstanceResolvers;
            _preInstanceResolvers = preInstanceResolvers;
        }

        [CanBeNull]
        public object CreateObject(Type type, ObjectDataContainer objectDataContainer)
        {
            var parameters = new List<object>();
            _preInstanceResolvers.ForEach(preInstanceResolver =>
            {
                parameters.AddRange(preInstanceResolver.Resolve(type, objectDataContainer));
            });
            var resultObject = Activator.CreateInstance(type, parameters.ToArray());
            VerificationExtensions.CheckParametersForNull(() =>
                    throw new ContainerException("ActivatorService can't create object", false),
                resultObject);
            _postInstanceResolvers.ForEach(postInstanceResolver =>
            {
                postInstanceResolver.Resolve(resultObject, objectDataContainer);
            });
            return resultObject;
        }
    }
}