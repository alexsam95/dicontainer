using System;

namespace  SimpleDIContainer.CoreLogic
{
    internal class ActivatorErrorHandleDecorator : IActivatorService
    {
        private readonly ActivatorService _activatorService;

        public ActivatorErrorHandleDecorator(ActivatorService activatorService)
        {
            _activatorService = activatorService;
        }

        public object CreateObject(Type type, ObjectDataContainer objectDataContainer)
        {
            return HandleError(type, () => _activatorService.CreateObject(type, objectDataContainer));
        }

        private object HandleError(Type type, Func<object> handledFunc)
        {
            try
            {
                return handledFunc?.Invoke();
            }
            catch (MissingMethodException)
            {
                throw new ContainerException("Error with creating: " + type +
                                             " -incorrect constructors params(bind and injected types are different)",
                    true);
            }
            catch (StackOverflowException)
            {
                throw new ContainerException("Error with create :" +
                                             type + " stack overflow exception - you use circular dependency", true);
            }
        }
    }
}