using System;
using SimpleDIContainer.DataLayer;
using SimpleDIContainer.UserLayer;

namespace SimpleDIContainer.CoreLogic.Resolvers
{
    internal class FactoryResolver : IPostInstanceResolver
    {
        private readonly IObjectsStorage<Type, ObjectDataContainer> _storage;

        internal FactoryResolver(IObjectsStorage<Type, ObjectDataContainer> storage)
        {
            _storage = storage;
        }

        public void Resolve(object concreteObj, ObjectDataContainer objectDataContainer)
        {
            if (!(concreteObj is IBindFactory factory)) return;
            var factoryResult = factory.GetConstructedObject();
            _storage.Add(factory.GetDeclaredType(), new ObjectDataContainer(factoryResult));
        }
    }
}