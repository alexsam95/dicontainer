using System;
using System.Collections.Generic;
using System.Text;
using SimpleDIContainer.Utils;

namespace SimpleDIContainer.CoreLogic.Resolvers
{
    internal class PreInstanceResolverLogDecorator : IPreInstanceResolver
    {
        private readonly ILoggerProvider _loggerProvider;
        private readonly IPreInstanceResolver _preInstanceResolver;
        public PreInstanceResolverLogDecorator(IPreInstanceResolver preInstanceResolver, ILoggerProvider loggerProvider)
        {
            _preInstanceResolver = preInstanceResolver;
            _loggerProvider = loggerProvider;
        }
        public void Dispose()
        {
            _preInstanceResolver.Dispose();
        }

        public IEnumerable<object> Resolve(Type type, ObjectDataContainer objectDataContainer)
        {
            _loggerProvider.Logger?.AddToLog("resolve before instance" + type);
            return _preInstanceResolver.Resolve(type, objectDataContainer);
        }
    }
}