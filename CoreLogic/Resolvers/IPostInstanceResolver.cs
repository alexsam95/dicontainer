namespace SimpleDIContainer.CoreLogic.Resolvers
{
    internal interface IPostInstanceResolver
    {
        void Resolve(object concreteObj, ObjectDataContainer objectDataContainer);
    }
}