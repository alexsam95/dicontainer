using SimpleDIContainer.Utils;

namespace SimpleDIContainer.CoreLogic.Resolvers
{
    internal class PostInstanceResolverLogDecorator : IPostInstanceResolver
    {
        private readonly IPostInstanceResolver _preInstanceResolver;
        private readonly ILoggerProvider _loggerProvider;
        internal PostInstanceResolverLogDecorator(IPostInstanceResolver preInstanceResolver, ILoggerProvider loggerProvider)
        {
            _preInstanceResolver = preInstanceResolver;
            _loggerProvider = loggerProvider;
        }
        public void Resolve(object concreteObj, ObjectDataContainer objectDataContainer)
        {
            _loggerProvider.Logger?.AddToLog("resolve post instance" + concreteObj);
            _preInstanceResolver.Resolve(concreteObj, objectDataContainer);
        }
    }
}