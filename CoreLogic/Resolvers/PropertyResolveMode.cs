namespace SimpleDIContainer.CoreLogic.Resolvers
{
    public enum PropertyResolveMode : byte
    {
        All,
        Manual
    }
}