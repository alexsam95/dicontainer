using System.Collections.Generic;
using SimpleDIContainer.CoreLogic;

namespace SimpleDIContainer.DataLayer
{
    internal interface IObjectsStorage<in TRegisterType, TObjectContainerType> where TObjectContainerType : ObjectDataContainer
    {
        void Add(TRegisterType type, TObjectContainerType objectDataContainer);
        IList<TObjectContainerType> GetObjectContainers(TRegisterType type);
        bool Contains(TRegisterType type);
        void Clear();
    }
}