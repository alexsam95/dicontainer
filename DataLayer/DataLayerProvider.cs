using System;
using SimpleDIContainer.CoreLogic;

namespace SimpleDIContainer.DataLayer
{
    internal class DataLayerProvider : IDataLayerProvider
    {
        public IObjectsStorage<Type, ObjectDataContainer> ObjectStorage => _storage.Value;

        private readonly Lazy<IObjectsStorage<Type, ObjectDataContainer>> _storage =
            new Lazy<IObjectsStorage<Type, ObjectDataContainer>>(() => new ObjectStorage());

        public void Dispose()
        {
            _storage.Value?.Clear();
        }
    }
}