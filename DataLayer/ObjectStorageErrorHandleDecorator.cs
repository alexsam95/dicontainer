using System;
using System.Collections.Generic;
using SimpleDIContainer.CoreLogic;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.DataLayer
{
    internal class ObjectStorageErrorHandleDecorator : IObjectsStorage<Type, ObjectDataContainer>
    {
        private readonly ObjectStorage _objectStorage;
        public ObjectStorageErrorHandleDecorator(ObjectStorage objectStorage)
        {
            _objectStorage = objectStorage;
        }

        public void Add(Type type, ObjectDataContainer objectDataContainer)
        {
            VerificationExtensions.CheckParametersForNull(() => throw new ContainerException(
                "Attempt add null data to storage",
                false), objectDataContainer);
            _objectStorage.Add(type, objectDataContainer);
        }

        public IList<ObjectDataContainer> GetObjectContainers(Type type)
        {
            return _objectStorage.GetObjectContainers(type);
        }

        public bool Contains(Type type)
        {
            return _objectStorage.Contains(type);
        }

        public void Clear()
        {
            _objectStorage.Clear();
        }
    }
}