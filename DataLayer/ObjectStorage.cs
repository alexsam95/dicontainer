using System;
using System.Collections.Generic;
using SimpleDIContainer.CoreLogic;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.DataLayer
{
    internal class ObjectStorage : IObjectsStorage<Type, ObjectDataContainer>
    {
        private readonly Dictionary<Type, IList<ObjectDataContainer>> _storage =
            new Dictionary<Type, IList<ObjectDataContainer>>();

        public void Add(Type type, ObjectDataContainer objectDataContainer)
        {
            var typesList = new List<Type>(objectDataContainer.AdditionalTypesForRegister.Count + 1) {type};
            typesList.AddRange(objectDataContainer.AdditionalTypesForRegister);
            typesList.ForEach(typeValue =>
            {
                if (_storage.ContainsKey(typeValue))
                {
                    AddRealizationToOtherRealizations(typeValue, objectDataContainer);
                    return;
                }

                _storage.Add(typeValue, new List<ObjectDataContainer> {objectDataContainer});
            });
        }

        public IList<ObjectDataContainer> GetObjectContainers(Type type)
        {
            if (!type.IsIEnumerableImplement())
            {
                return _storage.ContainsKey(type) ? _storage[type] : null;
            }

            var key = type.GenerateKey();
            return _storage.ContainsKey(key) ? _storage[key] : null;
        }

        public bool Contains(Type type)
        {
            return _storage.ContainsKey(!type.IsIEnumerableImplement()
                ? type
                : type.GenerateKey());
        }

        public void Clear()
        {
            _storage.Clear();
        }

        private void AddRealizationToOtherRealizations(Type type, ObjectDataContainer objectDataContainer)
        {
            var objectContainers = _storage[type];
            objectContainers.Add(objectDataContainer);
           var key = typeof(IEnumerable<>).MakeGenericType(type);
            if (!_storage.ContainsKey(key))
            {
                _storage.Add(key, objectContainers);
            }
            else
            {
                _storage[key] = objectContainers;
            }
        }
    }
}