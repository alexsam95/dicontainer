using System;
using SimpleDIContainer.CoreLogic;

namespace SimpleDIContainer.DataLayer
{
    internal interface IDataLayerProvider : IDisposable
    {
        IObjectsStorage<Type, ObjectDataContainer> ObjectStorage { get; }
    }
}