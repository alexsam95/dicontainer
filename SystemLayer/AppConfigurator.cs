using System;
using SimpleDIContainer.CoreLogic;
using SimpleDIContainer.DataLayer;
using SimpleDIContainer.Utils;

namespace SimpleDIContainer.SystemLayer
{
    internal class AppConfigurator : IDisposable
    {
        public ICoreLogicProvider CoreLogicProvider => _coreLogicProvider.Value;

        private readonly Lazy<IDataLayerProvider> _dataLayerProvider =
            new Lazy<IDataLayerProvider>(() => new DataLayerProvider());

        private readonly Lazy<ICoreLogicProvider> _coreLogicProvider;
        private readonly ILoggerProvider _loggerProvider;
        private readonly LoggerNullObject _loggerNullObject = new LoggerNullObject();
        public AppConfigurator(ILoggerProvider loggerProvider)
        {
            _loggerProvider = loggerProvider;
            _loggerProvider.Logger = _loggerNullObject;
            _coreLogicProvider = new Lazy<ICoreLogicProvider>(() => new CoreLogicProvider(GetDataLayerProvider(), loggerProvider));
        }

        public void BindLogger(IOperationLogger logger)
        {
            _loggerProvider.Logger = logger;
        }

        public void DisposeLogger()
        {
            _loggerProvider.Logger?.ShowLog();
            _loggerProvider.Logger = _loggerNullObject;
        }

        public void Dispose()
        {
            _coreLogicProvider.Value?.Dispose();
            _dataLayerProvider.Value?.Dispose();
        }

        private IDataLayerProvider GetDataLayerProvider()
        {
            return _dataLayerProvider.Value;
        }
    }
}