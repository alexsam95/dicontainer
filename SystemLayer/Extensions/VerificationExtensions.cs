using System;
using System.Linq;

namespace SimpleDIContainer.SystemLayer.Extensions
{
    internal static class VerificationExtensions
    {
        public static void CheckParametersForNull(Action handleNullAction, params object[] objects)
        {
            if (objects.All(obj => obj != null)) return;
            handleNullAction?.Invoke();
        }

        public static void CheckParametersForPredicate(Predicate<object> checkedPredicate,
            Action handleFalsePredicate,
            params object[] objects)
        {
            if (!objects.All(obj => checkedPredicate(obj)))
            {
                handleFalsePredicate?.Invoke();
            }
        }
    }
}