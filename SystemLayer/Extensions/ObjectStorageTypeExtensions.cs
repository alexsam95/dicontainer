using System;
using System.Linq;

namespace SimpleDIContainer.SystemLayer.Extensions
{
    internal static class ObjectStorageTypeExtensions
    {
        public static Type GenerateKey(this Type type)
        {
            return !type.IsGenericTypeDefinition ? type : type.MakeGenericType(type.GetGenericArguments());
        }

        public static bool IsIEnumerableImplement(this Type type)
        {
            return HasGenericType(type)
                   && type.GetInterfaces().Contains(GenerateKey(type));
        }

        private static bool HasGenericType(Type type)
        {
            return type.GetGenericArguments().Any();
        }
    }
}