using SimpleDIContainer.CoreLogic.CommandLogic;
using SimpleDIContainer.CoreLogic.Resolvers;

namespace SimpleDIContainer.UserLayer
{
    public class ObjectConfigurator
    {
        private readonly ICommandStorage _commandStorage;
        private readonly ICommandConfigurator _commandConfigurator;

        public ObjectConfigurator(ICommandStorage commandStorage, ICommandConfigurator commandConfigurator)
        {
            _commandConfigurator = commandConfigurator;
            _commandStorage = commandStorage;
        }

        /// <summary>
        /// Bind additional type for instance. You can get instance by this types
        /// </summary>
        public ObjectConfigurator AddAdditionalBindType<TAbstractType>()
        {
            _commandConfigurator.AddAdditionalType<TAbstractType>();
            return this;
        }
        /// <summary>
        /// Bind property in class. Container find property with type T and set dependency 
        /// </summary>
        public ObjectConfigurator BindProperty(string propertyName)
        {
            _commandConfigurator.AddPropertyTypeForBind(propertyName);
            return this;
        }

        /// <summary>
        /// Bind all property in class
        /// </summary>
        public ObjectConfigurator BindAllProperties()
        {
            _commandConfigurator.SetPropertyResolveMode(PropertyResolveMode.All);
            return this;
        }

        /// <summary>
        /// Bind method arguments. Container find method by methodname and get all dependency by argument
        /// </summary>
        public ObjectConfigurator BindMethod(string methodName)
        {
            _commandConfigurator.AddMethodNameForBind(methodName);
            return this;
        }

        /// <summary>
        /// Set configuration. Transient strategy: for each appeal container create a new instance 
        /// </summary>
        public ObjectConfigurator AsTransient()
        {
            _commandConfigurator.SetLifecycleType(LifecycleType.Transient);
            return this;
        }

        /// <summary>
        /// Set configuration. Transient strategy: for each appeal container returned one(cached) instance 
        /// </summary>
        public ObjectConfigurator AsSingle()
        {
            _commandConfigurator.SetLifecycleType(LifecycleType.Singleton);
            return this;
        }

        /// <summary>
        ///  Create this instance now 
        /// </summary>
        public void ActivateNonLazy()
        {
            _commandStorage.RunCommands();
        }

        /// <summary>
        ///  Manual (without container) add objects to constructor after container create this instance
        /// </summary>
        public ObjectConfigurator SetToConstructor(params object[] parameters)
        {
            _commandConfigurator.AddObjectsForConstructor(parameters);
            return this;
        }

        /// <summary>
        ///  Manual (without container) add objects to method's argument (find by method name) after container
        /// create this instance
        /// </summary>
        public ObjectConfigurator SetToMethod(string methodName, params object[] parameters)
        {
            _commandConfigurator.AddObjectsForMethod(methodName, parameters);
            return this;
        }

        /// <summary>
        /// Manual (without container) add objects to property (find by property name) after container
        /// create this instance
        /// </summary>
        /// <returns></returns>
        public ObjectConfigurator SetToProperty(string propertyName, object parameter)
        {
            _commandConfigurator.AddObjectsForProperty(propertyName, parameter);
            return this;
        }
    }
}