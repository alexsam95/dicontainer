using System;

namespace SimpleDIContainer.UserLayer
{
    public abstract class BindFactory <TInstanceType> : IBindFactory
    {
        public Type GetDeclaredType()
        {
            return typeof(TInstanceType);
        }
        public virtual object GetConstructedObject()
        {
            return ConstructObject();
        }

        protected abstract TInstanceType ConstructObject();
    }
}