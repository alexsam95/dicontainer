﻿using System;
using SimpleDIContainer.CoreLogic.CommandLogic;
using SimpleDIContainer.SystemLayer;
using SimpleDIContainer.Utils;

namespace SimpleDIContainer.UserLayer
{
    public class Container : IDisposable
    {
        private readonly AppConfigurator _appConfigurator;
        private ICommandStorage CommandStorage => _appConfigurator.CoreLogicProvider.CommandStorage;

        private ICommandConfigurator CommandConfigurator =>
            _appConfigurator.CoreLogicProvider.CommandConfigurator;

        private ObjectConfigurator _objectConfigurator;

        public Container()
        {
            ILoggerProvider loggerProvider = new LoggerProvider();
            _appConfigurator = new AppConfigurator(loggerProvider);
            _objectConfigurator = new ObjectConfigurator(CommandStorage, CommandConfigurator);
        }

        public void StartLog(IOperationLogger operationLogger)
        {
            _appConfigurator.BindLogger(operationLogger);
        }

        public void StopLog()
        {
            _appConfigurator.DisposeLogger();
        }

        /// <summary>
        /// Bind class as interface.The container gives this dependency on the interface type
        /// </summary>
        public ObjectConfigurator Bind<TRegisterType, TConcreteType>() where TConcreteType : class, TRegisterType
        {
            CommandStorage.AddCommand(typeof(TRegisterType), typeof(TConcreteType));
            return _objectConfigurator;
        }

        /// <summary>
        /// Bind class. The container gives this dependency on the class type
        /// </summary>
        public ObjectConfigurator BindInstance<TConcreteType>()
        {
            CommandStorage.AddCommand(typeof(TConcreteType), typeof(TConcreteType));
            return _objectConfigurator;
        }

        /// <summary>
        /// use this for register custom factory(returned object set to container)
        /// </summary>
        public ObjectConfigurator SetFactory<TConcreteType>()
        {
            CommandStorage.AddCommand(typeof(TConcreteType), typeof(TConcreteType));
            return _objectConfigurator;
        }

        /// <summary>
        /// set instance to container
        /// </summary>
        public ObjectConfigurator SetInstance<TRegisterType>(object instance)
        {
            CommandStorage.AddCommand(typeof(TRegisterType), instance);
            return _objectConfigurator;
        }

        /// <summary>
        /// manual clear resources(all created dependencies)
        /// </summary>
        public void Dispose()
        {
            _objectConfigurator = null;
            _appConfigurator.Dispose();
        }
    }
}