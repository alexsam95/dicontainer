namespace SimpleDIContainer.UserLayer
{
    public enum LifecycleType : byte
    {
        Singleton,
        Transient
    }
}