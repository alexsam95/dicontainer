using JetBrains.Annotations;

namespace SimpleDIContainer.Utils
{
    public class LoggerProvider : ILoggerProvider
    {
        [CanBeNull]
        public IOperationLogger Logger { get; set; }
    }
}