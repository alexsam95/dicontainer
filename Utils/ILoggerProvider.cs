namespace SimpleDIContainer.Utils
{
    public interface ILoggerProvider
    {
        IOperationLogger Logger { get; set; }
    }
}