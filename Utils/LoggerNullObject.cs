namespace SimpleDIContainer.Utils
{
    public class LoggerNullObject : IOperationLogger
    {
        private IOperationLogger _operationLogger;

        public void SetLogger(IOperationLogger logger)
        {
            _operationLogger = logger;
        }
        public void AddToLog(string operation)
        {
            _operationLogger?.AddToLog(operation);
        }

        public void ShowLog()
        {
            _operationLogger?.ShowLog();
        }
    }
}