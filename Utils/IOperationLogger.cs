namespace SimpleDIContainer.Utils
{
    public interface IOperationLogger
    {
        void AddToLog(string operation);
        void ShowLog();
    }
}